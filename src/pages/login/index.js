import React, { Component, Fragment } from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { Redirect } from 'react-router-dom';
import "./login.less";

//Layout
import LoginLayout from "../../components/layouts/loginLayout";

//Mutation
const LOGIN_MUTATION = gql`
    mutation Login( $input:LoginInput! ) {
        login( input: $input ) {
            authToken
            refreshToken
            user {
                id
                userId
                firstName
                lastName
                avatar {
                    url
                }
            }
        }
    }
`;

class LoginPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirectToReferrer: false,
            loading: false
        };

        this.login = this.login.bind(this);
        this.setLoginState = this.setLoginState.bind(this);
        this.setFormErrors = this.setFormErrors.bind(this);
    }

    setFormErrors = (error) => {
        const { form } = this.props;

        if (error.message === 'GraphQL error: incorrect_password') {
            form.setFields({
                password: {
                    value: null,
                    errors: [new Error('Contraseña inválida')]
                },
            });
        }

        if (error.message === 'GraphQL error: invalid_username') {
            form.setFields({
                username: {
                    value: null,
                    errors: [new Error('Usuario inválido')]
                },
            });
        }

        this.setState({ loading: false });
    };

    setLoginState = () => {
        this.setState({ redirectToReferrer: true, loading: false });
    };

    login = (data) => {
        if (data && data.login) {

            this.setLoginState();

            if (data.login.authToken) {
                localStorage.setItem('authToken', data.login.authToken);
            }
            if (data.login.refreshToken) {
                localStorage.setItem('refreshToken', data.login.refreshToken);
            }
        }
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const { redirectToReferrer } = this.state;

        if (redirectToReferrer) {
            return <Redirect to="/" />;
        }

        return (
            <LoginLayout>
                <div className="limiter">
                    <div className="container-login100">
                        <div className="wrap-login100">
                            <Mutation
                                mutation={LOGIN_MUTATION}
                                onError={error => this.setFormErrors(error)}
                                onCompleted={data => this.login(data)}
                                // onError={result => console.log("log", result)}
                                // onCompleted={result => console.log("log", result)}
                            >
                                {(login) => (
                                    <Fragment>
                                        <Form className="login-form login100-form validate-form" onSubmit={(e) => {
                                            e.preventDefault();

                                            localStorage.removeItem('authToken');
                                            localStorage.removeItem('refreshToken');

                                            this.props.form.validateFields((err, values) => {
                                                if (!err) {
                                                    this.setState({ loading: true });
                                                    login({
                                                        variables: {
                                                            input: {
                                                                username: values.username || null,
                                                                password: values.password || null,
                                                                clientMutationId: "Login"
                                                            }
                                                        }
                                                    })
                                                }
                                            });
                                        }}>
                                            <Form.Item className="wrap-input100 validate-input">
                                                {getFieldDecorator('username', {
                                                    rules: [{ required: true, message: 'Porfavor inserte su usuario' }],
                                                })(
                                                    <Input className="input100" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Usuario" />
                                                )}
                                            </Form.Item>
                                            <Form.Item className="wrap-input100 validate-input">
                                                {getFieldDecorator('password', {
                                                    rules: [{ required: true, message: 'Porfavor inserte su contraseña' }],
                                                })(
                                                    <Input className="input100" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Contraseña" />
                                                )}
                                            </Form.Item>
                                            <Form.Item>
                                                {getFieldDecorator('remember', {
                                                    valuePropName: 'checked',
                                                })(
                                                    <Checkbox>Recordarme</Checkbox>
                                                )}
                                            </Form.Item>
                                            <Form.Item>
                                                <div className="container-login100-form-btn">
                                                    <div className="wrap-login100-form-btn">
                                                        <div className="login100-form-bgbtn"></div>
                                                        <Button htmlType="submit" className="login100-form-btn" loading={this.state.loading}>
                                                            Iniciar Sesión
                                                        </Button>
                                                    </div>
                                                </div>
                                            </Form.Item>
                                            <center>
                                                <a className="txt2" href="/">Registrate Ahora!</a>
                                                <br></br>
                                                <a className="txt2" href="/"> Ovidaste password</a>
                                            </center>
                                        </Form>
                                    </Fragment>
                                )}
                            </Mutation>
                        </div>
                    </div>
                </div>
            </LoginLayout>
        );
    }
}

export default Form.create()(LoginPage);