import React, { Component } from 'react';
import { Form, Input, DatePicker, Select, Upload, Button, Icon, Radio, Row, Col } from 'antd';

const { Option } = Select;

class MockRegister extends Component {
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form
                onSubmit={e => {
                    e.preventDefault();

                    this.props.form.validateFieldsAndScroll((err, values) => {
                        if (!err) {
                            console.log('Received values of form: ', values);
                        }
                    });
                }}
            >
                <Form.Item label="Nombre y apellidos">
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please input your name' }]
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="Fecha de nacimiento">
                    {getFieldDecorator('bornDate', {
                        rules: [{ required: true, message: 'Please input your birth date' }]
                    })(
                        <DatePicker showTime format="YYYY-MM-DD" />
                    )}
                </Form.Item>
                <Form.Item label="Tipo de documento">
                    {getFieldDecorator('docType', {
                        rules: [{ required: true, message: 'Please input your doc type' }]
                    })(
                        <Select>
                            <Option value="cc">CC</Option>
                            <Option value="ti">TI</Option>
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="Número de identificación">
                    {getFieldDecorator('docNumber', {
                        rules: [{ required: true, message: 'Please input identification number' }]
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item label="Subir foto o video">
                    {getFieldDecorator('upload', {
                        valuePropName: 'fileList',
                        getValueFromEvent: this.normFile,
                    })(
                        <Upload name="logo" action="/upload.do" listType="picture">
                            <Button>
                                <Icon type="upload" /> Click to upload
                            </Button>
                        </Upload>
                    )}
                </Form.Item>
                <Form.Item label="Sexo">
                    {getFieldDecorator('sex', {
                        rules: [{ required: true, message: 'Please select you sex' }]
                    })(
                        <Radio.Group style={{ width: "50%" }}>
                            <Row>
                                <Col span={8}><Radio value="m">M</Radio></Col>
                                <Col span={8}><Radio value="f">F</Radio></Col>
                            </Row>
                        </Radio.Group>
                    )}
                </Form.Item>
                <Form.Item label="Pais nacimiento">
                    {getFieldDecorator('countryBorn', {
                        rules: [{ required: true, message: 'Please select your country!' }]
                    })(
                        <Select placeholder="Selecciona tu país de nacimiento">
                            <Option value="colombia">Colombia</Option>
                        </Select>
                    )}
                </Form.Item>
                <Form.Item label="Pais residencia">
                    {getFieldDecorator('currentCountry', {
                        rules: [{ required: true, message: 'Please select your current country!' }]
                    })(
                        <Select placeholder="Selecciona tu país de residencia">
                            <Option value="colombia">Colombia</Option>
                        </Select>
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">Registrar</Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create({ name: 'register' })(MockRegister);