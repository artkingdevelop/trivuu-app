import React, { Component, Fragment } from 'react';
import { Avatar } from 'antd';
import { Query } from 'react-apollo';
import { Redirect } from 'react-router-dom';
import gql from 'graphql-tag';

const CURRENT_USER_QUERY = gql`
    query CURRENT_USER {
        viewer {
            displayName: nickname
            avatar(size: 50) {
                url
            }
        }
    }
`;

export default class MockUser extends Component {
    render() {
        return(
            <Query
                query={CURRENT_USER_QUERY}
                onError={(errors) => {
                    localStorage.removeItem('authToken');
                    localStorage.removeItem('refreshToken');
                }}
            >
                {({loading, error, data}) => {
                    if (loading) return "loading...";
                    if (error) {
                        console.log(error);
                        return <Redirect to="/mock/login"/>
                    }
                
                    const { viewer } = data;
                    console.log(viewer);

                    if (!viewer) {
                        return <Redirect to="/mock/login" />;
                    }

                    return (
                        <Fragment>
                            <h2>Name: {viewer.displayName}</h2>
                            <Avatar size={64} src={viewer.avatar.url} />
                        </Fragment>
                    )
                }}
            </Query>
        );
    }
}