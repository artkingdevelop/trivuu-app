import React, { Component } from 'react';
import "./home.less";
import simbolo from '../../components/img/simbolo.png';
// layout
import HomeLayout from "../../components/layouts/homeLayout";
import SearchProducts from "../../components/search";


export default class HomePage extends Component {
    render() {
        return (
            <HomeLayout>
            	<section className="first-home" >
            		<div className="content-search-home">
            			<img className=" simbolo-search " alt="" src={simbolo} />
            			<SearchProducts></SearchProducts>
            			<h1>ENCUENTRA LOS MEJORES CONSULTORES, COACHES Y SPEAKERS DE ÁMERICA LATINA</h1>
            		</div>
            	</section>
                <h2>Content</h2><span></span>

            </HomeLayout> 
        );
    }
}	