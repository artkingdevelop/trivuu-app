import React, { Component } from 'react';
import { Row, Col, Upload, Form } from 'antd';

import LoginLayout from "../../components/layouts/loginLayout";
import RegisterStep from "../../components/register";
import "./register.less";

export default class RegisterPage extends Component {
  render() {
    return (
      <LoginLayout>
        <Row>
          <div className="content-register">
            <Col span={6}>
              <Upload
                name="avatar"
                listType="picture-card"
                className="avatar-uploader"
              >
                <img src="" alt="avatar" />
              </Upload>
            </Col>
            <Col span={18}>
              <Form onSubmit={this.handleSubmit} className="login-form register100-form validate-form">
                <RegisterStep/>
              </Form>
            </Col>
          </div>
        </Row>
      </LoginLayout>
    );
  }
}