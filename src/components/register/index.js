import React, { Component, Fragment } from 'react';
import { Layout, Steps, Button, message } from 'antd';

//Steps
import StepOneRegister from "./step-one";
import StepTwoRegister from "./step-two";

//Components
const Step = Steps.Step;

export default class RegisterStep extends Component {
    constructor(props) {
        super(props);

        this.state = {
            current: 0,
            steps: [{
                title: 'Datos personales',
                content: <StepOneRegister />,
            }, {
                title: 'Subir información',
                content: <StepTwoRegister />,
            }]
        };
    }

    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }


    render() {
        const { current, steps } = this.state;
        return (
            <Fragment>
                <Steps current={current}>
                    {steps.map(item => <Step key={item.title} title={item.title} />)}
                </Steps>
                <div style={{ maxHeight: '500px' }} className="steps-content">{steps[current].content}</div>
                <div className="steps-action">
                    {
                        current < steps.length - 1
                        && <Button type="primary" onClick={() => this.next()}>Siguiente</Button>
                    }
                    {
                        current === steps.length - 1
                        && <Button type="primary" onClick={() => message.success('Processing complete!')}>Registrar</Button>
                    }
                    {
                        current > 0
                        && (
                            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                                Anterior
                            </Button>
                        )
                    }
                </div>
            </Fragment>
        )
    }
}