import React, { Component } from 'react';
import { Row, Col, Form, Icon, Input, Radio, Checkbox, Select, DatePicker, Transfer } from 'antd';

// components
const Option = Select.Option;
const RadioGroup = Radio.Group;

export default class StepOneRegister extends Component {
    render() {
        return(
            <div className="container-register100">
                <div className="wrap-register100">
                    <Form.Item className="validate-input">
                        <Row>
                            <Col span={12}>
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Nombres" />
                            </Col>
                            <Col span={1}>
                                <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                    -
                                    </span></Col>
                            <Col span={11}>
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Apellidos" />
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item className="validate-input">
                        <Row>
                            <Col span={10}>
                                <Select
                                    showSearch
                                    placeholder="Tipo de Documento"
                                    optionFilterProp="children"
                                >
                                    <Option value="dni">D.N.I </Option>
                                    <Option value="pasaporte">Pasaporte</Option>
                                </Select>
                            </Col>
                            <Col span={1}>
                                <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                    -
                                    </span></Col>
                            <Col span={13}>
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)', width: '100%' }} />} placeholder="Numero de Indentificacion" />
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item className="validate-input">
                        <Row>
                            <Col span={12}>
                                <Input prefix={<Icon type="email" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="email" />
                            </Col>
                            <Col span={1}>
                                <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                    -
                                    </span></Col>
                            <Col span={11}>
                                <label className="ant-form-item-required" title="Genero" style={{ padding: '0px 15px 0px 0px' }}> Genero</label>
                                <RadioGroup >
                                    <Radio value={1}>Masculino</Radio>
                                    <Radio value={2}>Femenino</Radio>
                                </RadioGroup>
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item className="validate-input">
                        <Row>
                            <Col span={12}>
                                <Input className="" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            </Col>
                            <Col span={1}>
                                <span style={{ display: 'inline-block', width: '100%', textAlign: 'center' }}>
                                    -
                                    </span></Col>
                            <Col span={8}>
                                <Input className="" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Confirm Password" />
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item className="validate-input">
                        <Row>
                            <Col span={8}>
                                <Select
                                    showSearch
                                    placeholder="Pais de Residencia"
                                    optionFilterProp="children"
                                >
                                    <Option value="dni">D.N.I </Option>
                                    <Option value="pasaporte">Pasaporte</Option>
                                </Select>
                            </Col>
                            <Col span={8}>
                                <Select
                                    showSearch
                                    placeholder="Tipo de Documento"
                                    optionFilterProp="children"
                                >
                                    <Option value="dni">D.N.I </Option>
                                    <Option value="pasaporte">Pasaporte</Option>
                                </Select>
                            </Col>
                            <Col span={8}>
                                <div>
                                    <DatePicker />
                                </div>
                            </Col>
                        </Row>
                    </Form.Item>
                </div>
            </div>
        );
    }
}