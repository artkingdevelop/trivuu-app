import React, { Component } from 'react';
import { Row, Col, Form, Checkbox, Transfer } from 'antd';

export default class StepTwoRegister extends Component {
    render() {
        return (
            <div className="container-register100">
                <div className="wrap-register100">
                    <Form.Item className="validate-input">
                        <Row>
                            <Col span={20}>
                                <Transfer />
                            </Col>
                        </Row>
                    </Form.Item>
                    <Form.Item className="validate-input">
                        <Row>

                        </Row>
                    </Form.Item>
                    <Form.Item>
                        <Checkbox>Acepto terminos y condiciones</Checkbox>
                        <p> Al seleccionar esta opcion esta de acuerdo con el
                                    <a class="txt2" href=""> Tratamiento de Datos</a>
                        </p>
                    </Form.Item>
                </div>
            </div>
        );
    }
}