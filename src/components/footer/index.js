import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import "./footer.less";

const { Footer } = Layout;

export default class FooterHome extends React.Component {
    render() {
        return (
            <Footer style={{ padding: '30px', backgroundColor: 'gray' }}>
                <Fragment>
                    Copyright <Icon type="copyright" /> 2010
                </Fragment>
            </Footer>
        )
    }
}