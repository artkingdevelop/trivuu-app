import React from 'react';
import { Layout, Row, Col , Select , Button} from 'antd';
import "./search.less";

const Option = Select.Option;

// components
function handleChange(value) {
  console.log(`selected ${value}`);
}

function handleBlur() {
  console.log('blur');
}

function handleFocus() {
  console.log('focus');
}


export default class SearchProducts extends React.Component {
    render() {
        return(
            <Layout className="content-search">
                <Row>
                <Col  sm={10}  lg={10} xl={10}>
                    <Select 
                    className="selectsearch"
                        showSearch
                         placeholder="Especialidad "

                      >
                        <Option value="Diseño Grafico">Diseño Grafico</Option>
                        <Option value="Administracion">Administracion</Option>
                        <Option value="Dinero">Dinero</Option>
                      </Select>
                	
                	</Col>
                	<Col  sm={10}  lg={10} xl={10}>
                         <Select 
                         className="selectsearch"
                        showSearch
                       placeholder="Tipo "

                      >
                        <Option value="Coach">Coach</Option>
                        <Option value="Speaker">Speaker</Option>
                        <Option value="Consultor">Consultor</Option>
                      </Select>
                    
                	</Col>
                	
                    <Col   sm={4}  lg={4} xl={4}>
                        <Button type="primary" className="button-search" icon="search" />
                    
                    </Col>
                </Row>
            </Layout>
        )
    }
}