import React, { Component, Fragment } from 'react';
import { Layout } from 'antd';

// components
const { Content } = Layout;

export default class LoginLayout extends Component {
    render() {
        return (
            <Fragment>
                <Layout style={{ background: '#f0f2f5' }}>
               
                    <Content>{this.props.children}</Content>         
                </Layout>
            </Fragment>
        );
    }
}