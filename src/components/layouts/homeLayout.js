import React, { Component, Fragment } from 'react';
import { Layout } from 'antd';

// components
import HeaderHome from "../header";
import FooterHome from "../footer";
const { Content } = Layout;

export default class HomeLayout extends Component {
    render() {
        return (
            <Fragment>
                <Layout style={{ background: '#f0f2f5' }}>
                    <HeaderHome />
                    <Content>{this.props.children}</Content>
                    <FooterHome />
                </Layout>
            </Fragment>
        );
    }
}