import React from 'react';
import { Layout, Row, Col } from 'antd';
import "./header.less";
import logo from '../img/logo.png';

// components
const { Header } = Layout;

export default class HeaderHome extends React.Component {
    render() {
        return(
            <Header className="header-home">
                <Row>
                	<Col  sm={8}  lg={8} xl={4}>
                		sdasd
                	</Col>
                	<Col className="content-logo"  sm={12}  lg={8} xl={16}>
                		<img className="logo-header" src={logo} alt="" />
                	</Col>
                	<Col  sm={12} lg={8} xl={4}>
                		<div className=" content-cta ">
                            <h2 className="">¿ERES CONSULTOR, COACH O SPEAKER?</h2>
                            <p className="">SÚMATE YA</p>
                        </div>
                	</Col>
                </Row>
            </Header>
        )
    }
}