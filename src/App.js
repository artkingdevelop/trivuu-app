import React, { Component, Fragment } from 'react';
import { ApolloProvider } from "react-apollo";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { client } from "./apollo";

//Pages
import HomePage from "./pages/home";
import MainPage from "./pages/main";
import LoginPage from "./pages/login";
import RegisterPage from "./pages/register";

//Mock pages
import MockLogin from "./pages/mock/login";
import MockRegister from "./pages/mock/register";
import MockUser from "./pages/mock/user";

export default class App extends Component {
  render() {
    return(
      <ApolloProvider client={client}>
        <Fragment>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={HomePage}  />
              <Route exact path="/main" component={MainPage}  />
              <Route exact path="/login" component={LoginPage}  />
              <Route exact path="/register" component={RegisterPage}  />
              {/* mock routes */}
              <Route exact path="/mock/login" component={MockLogin} />
              <Route exact path="/mock/register" component={MockRegister} />
              <Route exact path="/mock/user" component={MockUser} />
            </Switch>
          </BrowserRouter>
        </Fragment>
      </ApolloProvider>
    )
  }
}
